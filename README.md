# Rfam

A simple File Alteration Monitor, implemented in JRuby. The core functionality of the gem
is provided by the Apache [Commons IO](https://commons.apache.org/proper/commons-io/) File Monitor component.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'rfam'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install rfam

## Usage

    $ rfam -p PATH

Changes to files and directories at and under the path are reported to stdout.

## License

The gem is available as open source under the terms of the [Apache 2.0 license](https://opensource.org/licenses/Apache-2.0)
