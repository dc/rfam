require 'rfam/version'
require 'rfam_jars'
require 'json'

module Rfam

  include_package 'org.apache.commons.io.monitor'

  Alteration = Struct.new(:path, :filetype, :alteration, :timestamp) do
    def to_s
      JSON.dump(to_h)
    end
  end

end

require 'rfam/monitor'
