# this is a generated file, to avoid over-writing it just delete this comment
begin
  require 'jar_dependencies'
rescue LoadError
  require 'commons-io/commons-io/2.6/commons-io-2.6.jar'
end

if defined? Jars
  require_jar 'commons-io', 'commons-io', '2.6'
end
