module Rfam
  class Listener
    include Rfam::FileAlterationListener

    def onDirectoryChange(path)
      puts Alteration.new(path, :directory, :change, now)
    end

    def onDirectoryCreate(path)
      puts Alteration.new(path, :directory, :create, now)
    end

    def onDirectoryDelete(path)
      puts Alteration.new(path, :directory, :delete, now)
    end

    def onFileChange(path)
      puts Alteration.new(path, :file, :change, now)
    end

    def onFileCreate(path)
      puts Alteration.new(path, :file, :create, now)
    end

    def onFileDelete(path)
      puts Alteration.new(path, :file, :delete, now)
    end

    def onStart(observer)
      # no-op
    end

    def onStop(observer)
      # no-op
    end

    private

    def now
      Time.now
    end
  end
end
