require 'rfam/listener'

module Rfam
  class Monitor

    DEFAULT_INTERVAL = 1000

    def self.start(*args)
      new(*args).start
    end

    def self.run(*args)
      new(*args).run
    end

    attr_reader :path, :interval, :observer, :monitor

    def initialize(path:, interval: nil)
      @path = path
      @interval = interval || DEFAULT_INTERVAL
      @observer = Rfam::FileAlterationObserver.new(path)
      @observer.addListener(Listener.new)
      @monitor = Rfam::FileAlterationMonitor.new(@interval, @observer)
    end

    def start
      monitor.start
    end

    def stop
      monitor.stop(0)
    end

    def run
      begin
        start
        sleep
      ensure
        stop
      end
    end

  end
end
