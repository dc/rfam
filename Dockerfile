FROM jruby:9.2.7.0

RUN apt-get update -qq \
	&& apt-get install -y \
	vim less emacs-nox

WORKDIR /usr/src/rfam

COPY . .
RUN gem install bundler -v '2.0.1' \
	&& bundle install \
	&& rake rfam:install

CMD ["/bin/bash"]
