lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rfam/version"

Gem::Specification.new do |spec|
  spec.name          = "rfam"
  spec.version       = Rfam::VERSION
  spec.authors       = ["David Chandek-Stark"]
  spec.email         = ["david.chandek.stark@duke.edu"]

  spec.summary       = "A simple file alteration monitor, implemented in JRuby."
  spec.homepage      = "https://gitlab.oit.duke.edu/dc/rfam"
  spec.license       = "Apache-2.0"
  spec.platform      = "java"

  spec.files = Dir['lib/**/*.rb']
  spec.files += Dir['*file']
  spec.files += Dir['*.gemspec']

  spec.executables = ["rfam"]
  spec.require_paths = ["lib"]

  spec.requirements << "jar commons-io, commons-io, 2.6"

  spec.add_development_dependency "bundler", "~> 2.0"
  spec.add_development_dependency "rake", "~> 12.0"
end
